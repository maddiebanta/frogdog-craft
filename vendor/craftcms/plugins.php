<?php

$vendorDir = dirname(__DIR__);
$rootDir = dirname(dirname(__DIR__));

return array (
  'craftcms/redactor' => 
  array (
    'class' => 'craft\\redactor\\Plugin',
    'basePath' => $vendorDir . '/craftcms/redactor/src',
    'handle' => 'redactor',
    'aliases' => 
    array (
      '@craft/redactor' => $vendorDir . '/craftcms/redactor/src',
    ),
    'name' => 'Redactor',
    'version' => '2.3.3.2',
    'description' => 'Edit rich text content in Craft CMS using Redactor by Imperavi.',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/redactor/blob/v2/README.md',
  ),
  'sebastianlenz/linkfield' => 
  array (
    'class' => 'typedlinkfield\\Plugin',
    'basePath' => $vendorDir . '/sebastianlenz/linkfield/src',
    'handle' => 'typedlinkfield',
    'aliases' => 
    array (
      '@typedlinkfield' => $vendorDir . '/sebastianlenz/linkfield/src',
    ),
    'name' => 'Typed link field',
    'version' => '1.0.19',
    'description' => 'A Craft field type for selecting links',
    'developer' => 'Sebastian Lenz',
    'developerUrl' => 'https://github.com/sebastian-lenz/',
  ),
  'craftcms/feed-me' => 
  array (
    'class' => 'craft\\feedme\\Plugin',
    'basePath' => $vendorDir . '/craftcms/feed-me/src',
    'handle' => 'feed-me',
    'aliases' => 
    array (
      '@craft/feedme' => $vendorDir . '/craftcms/feed-me/src',
    ),
    'name' => 'Feed Me',
    'version' => '4.1.0',
    'description' => 'Import content from XML, RSS, CSV or JSON feeds into entries, categories, Craft Commerce products, and more.',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://docs.craftcms.com/feed-me/v4/',
  ),
  'lukeyouell/craft-mailchimplists' => 
  array (
    'class' => 'lukeyouell\\mailchimplists\\MailchimpLists',
    'basePath' => $vendorDir . '/lukeyouell/craft-mailchimplists/src',
    'handle' => 'mailchimp-lists',
    'aliases' => 
    array (
      '@lukeyouell/mailchimplists' => $vendorDir . '/lukeyouell/craft-mailchimplists/src',
    ),
    'name' => 'MailChimp Lists',
    'version' => '1.0.5',
    'description' => 'Create, manage and monitor your MailChimp Lists from within Craft.',
    'developer' => 'Luke Youell',
    'developerUrl' => 'https://github.com/lukeyouell',
    'documentationUrl' => 'https://github.com/lukeyouell/craft-mailchimplists/blob/v1/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/lukeyouell/craft-mailchimplists/v1/CHANGELOG.md',
    'hasCpSettings' => true,
    'hasCpSection' => true,
    'components' => 
    array (
      'mailchimpListsService' => 'lukeyouell\\mailchimplists\\services\\MailchimpListsService',
    ),
  ),
  'verbb/expanded-singles' => 
  array (
    'class' => 'verbb\\expandedsingles\\ExpandedSingles',
    'basePath' => $vendorDir . '/verbb/expanded-singles/src',
    'handle' => 'expanded-singles',
    'aliases' => 
    array (
      '@verbb/expandedsingles' => $vendorDir . '/verbb/expanded-singles/src',
    ),
    'name' => 'Expanded Singles',
    'version' => '1.0.7',
    'schemaVersion' => '1.0.0',
    'description' => 'Alters the Entries Index sidebar to list all Singles, rather than grouping them under a \'Singles\' link.',
    'developer' => 'Verbb',
    'developerUrl' => 'https://verbb.io',
    'developerEmail' => 'support@verbb.io',
    'documentationUrl' => 'https://github.com/verbb/expanded-singles',
    'changelogUrl' => 'https://raw.githubusercontent.com/verbb/expanded-singles/craft-3/CHANGELOG.md',
    'hasCpSettings' => true,
    'hasCpSection' => false,
  ),
  'madebyraygun/trending-posts' => 
  array (
    'class' => 'madebyraygun\\trendingposts\\TrendingPosts',
    'basePath' => $vendorDir . '/madebyraygun/trending-posts/src',
    'handle' => 'trending-posts',
    'aliases' => 
    array (
      '@madebyraygun/trendingposts' => $vendorDir . '/madebyraygun/trending-posts/src',
    ),
    'name' => 'Trending Posts',
    'version' => '1.0.1',
    'description' => 'Order posts by pageviews over time',
    'developer' => 'Raygun',
    'developerUrl' => 'https://madebyraygun.com',
    'documentationUrl' => 'https://github.com/madebyraygun/trending-posts/blob/master/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/madebyraygun/trending-posts/master/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => false,
    'components' => 
    array (
      'trendingPostsService' => 'madebyraygun\\trendingposts\\services\\TrendingPostsService',
    ),
  ),
  'putyourlightson/craft-entry-count' => 
  array (
    'class' => 'putyourlightson\\entrycount\\EntryCount',
    'basePath' => $vendorDir . '/putyourlightson/craft-entry-count/src',
    'handle' => 'entry-count',
    'aliases' => 
    array (
      '@putyourlightson/entrycount' => $vendorDir . '/putyourlightson/craft-entry-count/src',
    ),
    'name' => 'Entry Count',
    'version' => '2.0.2',
    'schemaVersion' => '2.0.0',
    'description' => 'Counts and displays the number of times that an entry has been viewed.',
    'developer' => 'PutYourLightsOn',
    'developerUrl' => 'https://www.putyourlightson.net/',
    'documentationUrl' => 'https://github.com/putyourlightson/craft-entry-count',
    'changelogUrl' => 'https://raw.githubusercontent.com/putyourlightson/craft-entry-count/v2/CHANGELOG.md',
    'hasCpSettings' => true,
    'hasCpSection' => true,
  ),
  'indigoviking/liquid-letters' => 
  array (
    'class' => 'indigoviking\\liquidletters\\LiquidLetters',
    'basePath' => $vendorDir . '/indigoviking/liquid-letters/src',
    'handle' => 'liquid-letters',
    'aliases' => 
    array (
      '@indigoviking/liquidletters' => $vendorDir . '/indigoviking/liquid-letters/src',
    ),
    'name' => 'Liquid Letters',
    'version' => '1.2',
    'description' => 'Count words, get reading times, and convert text to list items.',
    'developer' => 'The Indigo Viking',
    'developerUrl' => 'https://www.theindigoviking.com',
    'documentationUrl' => 'https://github.com/IndigoViking/liquid-letters/blob/master/README.md',
    'changelogUrl' => 'https://raw.githubusercontent.com/IndigoViking/liquid-letters/master/CHANGELOG.md',
    'hasCpSettings' => false,
    'hasCpSection' => false,
  ),
  'verbb/super-table' => 
  array (
    'class' => 'verbb\\supertable\\SuperTable',
    'basePath' => $vendorDir . '/verbb/super-table/src',
    'handle' => 'super-table',
    'aliases' => 
    array (
      '@verbb/supertable' => $vendorDir . '/verbb/super-table/src',
    ),
    'name' => 'Super Table',
    'version' => '2.2.1',
    'description' => 'Super-charge your Craft workflow with Super Table. Use it to group fields together or build complex Matrix-in-Matrix solutions.',
    'developer' => 'Verbb',
    'developerUrl' => 'https://verbb.io',
    'developerEmail' => 'support@verbb.io',
    'documentationUrl' => 'https://github.com/verbb/super-table',
    'changelogUrl' => 'https://raw.githubusercontent.com/verbb/super-table/craft-3/CHANGELOG.md',
  ),
  'craftcms/contact-form' => 
  array (
    'class' => 'craft\\contactform\\Plugin',
    'basePath' => $vendorDir . '/craftcms/contact-form/src',
    'handle' => 'contact-form',
    'aliases' => 
    array (
      '@craft/contactform' => $vendorDir . '/craftcms/contact-form/src',
    ),
    'name' => 'Contact Form',
    'version' => '2.2.5',
    'description' => 'Add a simple contact form to your Craft CMS site',
    'developer' => 'Pixel & Tonic',
    'developerUrl' => 'https://pixelandtonic.com/',
    'developerEmail' => 'support@craftcms.com',
    'documentationUrl' => 'https://github.com/craftcms/contact-form/blob/v2/README.md',
    'components' => 
    array (
      'mailer' => 'craft\\contactform\\Mailer',
    ),
  ),
  'nystudio107/craft-retour' => 
  array (
    'class' => 'nystudio107\\retour\\Retour',
    'basePath' => $vendorDir . '/nystudio107/craft-retour/src',
    'handle' => 'retour',
    'aliases' => 
    array (
      '@nystudio107/retour' => $vendorDir . '/nystudio107/craft-retour/src',
    ),
    'name' => 'Retour',
    'version' => '3.1.23',
    'description' => 'Retour allows you to intelligently redirect legacy URLs, so that you don\'t lose SEO value when rebuilding & restructuring a website',
    'developer' => 'nystudio107',
    'developerUrl' => 'https://nystudio107.com/',
    'documentationUrl' => 'https://nystudio107.com/plugins/retour/documentation',
    'changelogUrl' => 'https://raw.githubusercontent.com/nystudio107/craft-retour/v1/CHANGELOG.md',
    'hasCpSettings' => true,
    'hasCpSection' => true,
    'components' => 
    array (
      'redirects' => 'nystudio107\\retour\\services\\Redirects',
      'statistics' => 'nystudio107\\retour\\services\\Statistics',
    ),
  ),
  'nystudio107/craft-seomatic' => 
  array (
    'class' => 'nystudio107\\seomatic\\Seomatic',
    'basePath' => $vendorDir . '/nystudio107/craft-seomatic/src',
    'handle' => 'seomatic',
    'aliases' => 
    array (
      '@nystudio107/seomatic' => $vendorDir . '/nystudio107/craft-seomatic/src',
    ),
    'name' => 'SEOmatic',
    'version' => '3.2.21',
    'description' => 'SEOmatic facilitates modern SEO best practices & implementation for Craft CMS 3. It is a turnkey SEO system that is comprehensive, powerful, and flexible.',
    'developer' => 'nystudio107',
    'developerUrl' => 'https://nystudio107.com',
    'documentationUrl' => 'https://nystudio107.com/plugins/seomatic/documentation',
    'changelogUrl' => 'https://raw.githubusercontent.com/nystudio107/craft-seomatic/v3/CHANGELOG.md',
    'hasCpSettings' => true,
    'hasCpSection' => true,
    'components' => 
    array (
      'frontendTemplates' => 'nystudio107\\seomatic\\services\\FrontendTemplates',
      'helper' => 'nystudio107\\seomatic\\services\\Helper',
      'jsonLd' => 'nystudio107\\seomatic\\services\\JsonLd',
      'link' => 'nystudio107\\seomatic\\services\\Link',
      'metaBundles' => 'nystudio107\\seomatic\\services\\MetaBundles',
      'metaContainers' => 'nystudio107\\seomatic\\services\\MetaContainers',
      'seoElements' => 'nystudio107\\seomatic\\services\\SeoElements',
      'script' => 'nystudio107\\seomatic\\services\\Script',
      'sitemaps' => 'nystudio107\\seomatic\\services\\Sitemaps',
      'tag' => 'nystudio107\\seomatic\\services\\Tag',
      'title' => 'nystudio107\\seomatic\\services\\Title',
    ),
  ),
  'solspace/craft-freeform' => 
  array (
    'class' => 'Solspace\\Freeform\\Freeform',
    'basePath' => $vendorDir . '/solspace/craft-freeform/src',
    'handle' => 'freeform',
    'aliases' => 
    array (
      '@Solspace/Freeform' => $vendorDir . '/solspace/craft-freeform/src',
    ),
    'name' => 'Freeform',
    'version' => '3.3.1',
    'schemaVersion' => '3.0.3',
    'description' => 'The most reliable, intuitive and powerful form builder for Craft.',
    'developer' => 'Solspace',
    'developerUrl' => 'https://docs.solspace.com/craft/freeform/v3/',
    'documentationUrl' => 'https://docs.solspace.com/craft/freeform/v3/',
    'changelogUrl' => 'https://raw.githubusercontent.com/solspace/craft3-freeform/v3/CHANGELOG.md',
    'hasCpSection' => true,
  ),
);
